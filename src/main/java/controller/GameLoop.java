package main.java.controller;

import main.java.view.Displayable;
import main.java.view.GameViewImpl;

public interface GameLoop {
	
	/**starts the GameLoop thread.
	 * 
	 */
	void start();
	
	/**
	 * when called changes the status of the game from going to pause
	 * and viceversa.
	 * 
	 */
	void togglePause();
	
	/**
	 * when called returns true if the game is in pause,
	 * false if is going.
	 *
	 * @return the current state of the game
	 * 
	 */
	boolean isPaused();
	
	/**
	 * when called returns true if the gameLoop is runnig, 
	 * false if it's not yet started.
	 *
	 * @returns the current state of the gameLoop
	 * 
	 */
	boolean isRunning();
	
	/**
	 * when called sets s as the view of the game and
	 * it associate it with the gameLoop.
	 *
	 * @param s the view of the game to set
	 */
	void setView(GameViewImpl s);
	
	/**
	 * getter for the view 
	 * this view needs to be set right after the start of the game loop.
	 * 
	 * @return the game view
	 */
	Displayable getView();
}

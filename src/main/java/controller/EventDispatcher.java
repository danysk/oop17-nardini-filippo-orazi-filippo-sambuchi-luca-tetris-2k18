package main.java.controller;

import java.util.List;

import main.java.model.GameEvent;

/**
 * Functional interface for an event dispatcher, it takes a game 
 * event and triggers an action.
 */
@FunctionalInterface
public interface EventDispatcher {

  /**
   * Dispatches an action given an event.
   */
	void dispatch(GameEvent e);
	
	/**
	 * Dispatches an action for every event in the list.
	 * @param events
	 */
	default void dispatchAll(List<GameEvent> events) {
	  events.forEach(this::dispatch);
	}
}

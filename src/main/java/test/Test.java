package main.java.test;

import static main.java.model.Tetromino.Direction.DOWN;
import static main.java.model.Tetromino.Direction.LEFT;
import static main.java.model.Tetromino.Direction.RIGHT;
import static main.java.model.Tetromino.RotationSense.CLOCKWISE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import main.java.model.Point2D;
import main.java.model.Shape;
import main.java.model.Square;
import main.java.model.SquareColor;
import main.java.model.TetrisImpl;
import main.java.model.Tetromino;
import main.java.model.TetrominoGenerator;
import main.java.model.TetrominoImpl;


public class Test {

	@org.junit.Test
	public void testIsoutOfBounsMove() {
		final TetrisImpl t = new TetrisImpl(10, 20);
		t.setTetrominoCurrent(Shape.I);
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(3, 0, SquareColor.AZURE), 
				              new Square(4, 0, SquareColor.AZURE),
				              new Square(5, 0, SquareColor.AZURE), 
				              new Square(6, 0, SquareColor.AZURE)));
		t.move(RIGHT);
		t.move(RIGHT);
		t.move(RIGHT);
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(6, 0, SquareColor.AZURE), 
				              new Square(7, 0, SquareColor.AZURE),
				              new Square(8, 0, SquareColor.AZURE),
				              new Square(9, 0, SquareColor.AZURE)));
		t.move(RIGHT);
		/* test touch board right */
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(6, 0, SquareColor.AZURE), 
				              new Square(7, 0, SquareColor.AZURE),
				              new Square(8, 0, SquareColor.AZURE), 
				              new Square(9, 0, SquareColor.AZURE)));
		for (int i = 0; i < 100; i++) {
			t.move(LEFT);
		}

		/* test touch board left */
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(0, 0, SquareColor.AZURE), 
				              new Square(1, 0, SquareColor.AZURE),
				              new Square(2, 0, SquareColor.AZURE), 
				              new Square(3, 0, SquareColor.AZURE)));
		/* Anchor */
		for (int i = 0; i < 20; i++) {
			t.move(DOWN);
		}

		/* test touch the board down and then anchor the piece */
		assertEquals(t.getAnchoredPieces(),
				Arrays.asList(new Square(0, 19, SquareColor.AZURE), 
				              new Square(1, 19, SquareColor.AZURE),
				              new Square(2, 19, SquareColor.AZURE), 
				              new Square(3, 19, SquareColor.AZURE)));

	}

	@org.junit.Test
	public void testIsOutOfBoundsRotate() {
		final TetrisImpl t = new TetrisImpl(10, 20);
		t.setTetrominoCurrent(Shape.I);
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(3, 0, SquareColor.AZURE), 
				              new Square(4, 0, SquareColor.AZURE),
				              new Square(5, 0, SquareColor.AZURE), 
				              new Square(6, 0, SquareColor.AZURE)));
		t.rotate(CLOCKWISE);
		/* Not rotation becouse the tetromino is out of bounds : [5,-1 5,0 5,1 5,2] */
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(3, 0, SquareColor.AZURE), 
				              new Square(4, 0, SquareColor.AZURE),
				              new Square(5, 0, SquareColor.AZURE), 
				              new Square(6, 0, SquareColor.AZURE)));
		t.move(DOWN);
		t.rotate(CLOCKWISE);
		/* normal rotation */
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(5, 0, SquareColor.AZURE), 
				              new Square(5, 1, SquareColor.AZURE),
				              new Square(5, 2, SquareColor.AZURE), 
				              new Square(5, 3, SquareColor.AZURE)));
	}

	@org.junit.Test
	public void testCollision() {
		final TetrisImpl t = new TetrisImpl(10, 20);
		t.setTetrominoCurrent(Shape.I);

		/* Anchor */
		for (int i = 0; i < 20; i++) {
			t.move(DOWN);
		}
		assertEquals(t.getAnchoredPieces(),
				Arrays.asList(new Square(3, 19, SquareColor.AZURE), 
				              new Square(4, 19, SquareColor.AZURE),
				              new Square(5, 19, SquareColor.AZURE), 
				              new Square(6, 19, SquareColor.AZURE)));

		/* the tetromino is anchor and start a new tetromino */
		t.setTetrominoCurrent(Shape.I);
		for (int i = 0; i < 18; i++) {
			t.move(DOWN);
		}
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(3, 18, SquareColor.AZURE), 
				              new Square(4, 18, SquareColor.AZURE),
				              new Square(5, 18, SquareColor.AZURE), 
				              new Square(6, 18, SquareColor.AZURE)));
		/* the tetromino is anchor */
		t.move(DOWN);
		/*
		 * It doesn't go down because there is a collision and the coordinates of the
		 * current tetromino are the same of the new tetromino
		 */
		assertFalse(t.getCurrentTetromino().getAllSquares().get(0)
		              .equals(new Square(3, 10, SquareColor.AZURE)));

	}

	@org.junit.Test
	public void testLineElimination() {
		final TetrisImpl t = new TetrisImpl(10, 20);
		t.setTetrominoCurrent(Shape.I);

		/* Anchor */
		for (int i = 0; i < 20; i++) {
			t.move(DOWN);
		}
		/* getAnchoredPieces */
		assertEquals(t.getAnchoredPieces(),
				Arrays.asList(new Square(3, 19, SquareColor.AZURE), 
				              new Square(4, 19, SquareColor.AZURE),
				              new Square(5, 19, SquareColor.AZURE), 
				              new Square(6, 19, SquareColor.AZURE)));
		t.setTetrominoCurrent(Shape.T);
		for (int i = 0; i < 5; i++) {
			t.move(LEFT);
		}
		/* Anchor */
		for (int i = 0; i < 20; i++) {
			t.move(DOWN);
		}

		t.setTetrominoCurrent(Shape.T);
		for (int i = 0; i < 5; i++) {
			t.move(RIGHT);
		}

		assertTrue(t.getLines() == 0);

		/* Anchor And Elimination of the last Line */
		for (int i = 0; i < 20; i++) {
			t.move(DOWN);
		}

		/*
		 * the last line has been eliminated and all the other pieces went down of one
		 * position in fact into the board remains only two points
		 */
		assertEquals(t.getAnchoredPieces(),
				Arrays.asList(new Square(1, 19, SquareColor.VIOLET), 
				              new Square(8, 19, SquareColor.VIOLET)));
		assertTrue(t.getLines() == 1);

	}

	@org.junit.Test
	public void testHold() {
		final TetrisImpl t = new TetrisImpl(10, 20);
		t.setTetrominoCurrent(Shape.I);
		assertEquals(t.getHold(), Optional.empty());
		t.hold();
		assertEquals(t.getHold().get(), Shape.I);
		t.setTetrominoCurrent(Shape.T);
		/* swap the shape current with shape into "hold" */
		t.hold();
		assertEquals(t.getHold().get(), Shape.T);
		assertEquals(t.getCurrentTetromino().getShape(), Shape.I);

		/* I can't swap two times for the same current tetromino */
		t.hold();
		assertEquals(t.getHold().get(), Shape.T);
		assertEquals(t.getCurrentTetromino().getShape(), Shape.I);

	}

	@org.junit.Test
	public void TestMove() {
		TetrominoGenerator tg = new TetrominoGenerator(4);
		Tetromino t = tg.newTetromino(Shape.I);
		List<Square> l = t.getAllSquares();
		t.move(Tetromino.Direction.DOWN);
		List<Square> l2 = t.getAllSquares();
		l2.forEach(s -> {
			assertTrue(s.getCoords().getX() == l.get(l2.indexOf(s)).getCoords().getX());
			assertTrue(s.getCoords().getY()  
			                == l.get(l2.indexOf(s)).getCoords().getY() + 1);
		});

		t = tg.newTetromino(Shape.I);
		t.move(Tetromino.Direction.LEFT);
		List<Square> l3 = t.getAllSquares();
		l3.stream().peek(s -> {
			assertTrue(s.getCoords().getX() 
			              == l.get(l3.indexOf(s)).getCoords().getX() - 1);
			assertTrue(s.getCoords().getY() == l.get(l3.indexOf(s)).getCoords().getY());
		});

		t = tg.newTetromino(Shape.I);
		t.move(Tetromino.Direction.RIGHT);
		List<Square> l4 = t.getAllSquares();
		l4.forEach(s -> {
			assertTrue(s.getCoords().getX() 
			            == l.get(l4.indexOf(s)).getCoords().getX() + 1);
			assertTrue(s.getCoords().getY() == l.get(l4.indexOf(s)).getCoords().getY());
		});

	}

	@org.junit.Test
	public void testRotation() {
		TetrominoGenerator tg = new TetrominoGenerator(4);
		Tetromino t = tg.newTetromino(Shape.S);
		List<Square> l = t.getAllSquares();
		t.rotate(Tetromino.RotationSense.CLOCKWISE);
		List<Square> l2 = t.getAllSquares();
		l2.forEach(s -> {
			assertTrue(s.getCoords().getX() == 1 - (l.get(l2.indexOf(s)).getCoords().getY() - (4 - 2)));
			assertTrue(s.getCoords().getY() == l.get(l2.indexOf(s)).getCoords().getX() - 1);
		});

		t = tg.newTetromino(Shape.L);
		List<Square> l1 = t.getAllSquares();
		t.rotate(Tetromino.RotationSense.CLOCKWISE);
		List<Square> l3 = t.getAllSquares();
		l3.forEach(s -> {
			assertTrue(s.getCoords().getX() == 1 - (l1.get(l3.indexOf(s)).getCoords().getY() - (4 - 2)));
			assertTrue(s.getCoords().getY() == l1.get(l3.indexOf(s)).getCoords().getX() - 1);
		});

	}

	@org.junit.Test
	public void testBuilder() {
		try {
			new TetrominoImpl.Builder().build();
			fail("no exception");
		} catch (IllegalStateException e) {
			try {
				new TetrominoImpl.Builder().addPoint(new Point2D(0, 0)).addPoint(new Point2D(0, 1))
						.addPoint(new Point2D(1, 1)).addPoint(new Point2D(2, 1)).addPoint(new Point2D(0, 3));
				fail("no exception");
			} catch (IllegalArgumentException ex) {

			}
			try {
				new TetrominoImpl.Builder().addPoint(new Point2D(0, 1)).addPoint(new Point2D(0, 1));
			} catch (IllegalArgumentException ex) {

			}
		}
	}
}

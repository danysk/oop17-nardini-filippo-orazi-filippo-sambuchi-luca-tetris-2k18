package main.java;

import javafx.application.Application;
import main.java.view.MainApplication;

/**
 * Application entry point.
 */
public class Main {

  /**
   * It launches the JavaFX entry point.
   */
	public static void main(String[] args) {

		Application.launch(MainApplication.class, args);
	}
}

package main.java.model;

import java.util.List;
import java.util.Optional;

/**
 * Interface of the Tetris game. It is the core of the model part of the
 * application. It gives the methods to query the state of a Tetris game.
 */
public interface Tetris {

	/**
	 * Getter for the current score.
	 */
	int getScore();

	/**
	 * Getter for the cleared lines.
	 */
	int getLines();

	/**
	 * Getter for the current level.
	 */
	int getCurrentLevel();

	/**
	 * Getter for the current tetromino.
	 */
	Tetromino getCurrentTetromino();

	/**
	 * Getter for the game board including the current tetromino.
	 */
	List<Square> getBoard();

	/**
	 * Getter for the queue.
	 */
	List<Shape> getNext();

	/**
	 * Getter for the held tetromino.
	 */
	Optional<Shape> getHold();

	/**
	 * Getter for the list of events that happened into the game.
	 */
	List<GameEvent> getEvents();
	
	/**
	 * Checks if the game is over.
	 */
	boolean isGameOver();

	/**
	 * Sends the input to the game.
	 */
	void sendInput(final List<Input> inputs);

	/**
	 * Function that updates the state of the game.
	 * @param time	time elapsed
	 */

	void update(final double time);

	/**
	 * Returns all the anchored pieces, excluding the current tetromino.
	 */
	List<Square> getAnchoredPieces();
}

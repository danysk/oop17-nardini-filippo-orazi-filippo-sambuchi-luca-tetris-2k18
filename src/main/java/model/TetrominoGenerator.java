package main.java.model;

import java.lang.IllegalArgumentException;

/**
 * a Generator of Tetrominos, it makes easier to obtain a tetromino 
 * already in the standard position (SRS) 
 * and with the standard rotation.
 */
public class TetrominoGenerator {
	private int width;
	
	/** class contructor, it saves the width of the matrix where it will spawn the tetromino
	 *  in order to automatic set the starting point of the tetromino. 
	 *  It assumes that the grid is big enuogh to contain the tetromino.
	 * 
	 * @param 	width width of the grid where the generator will put the Tetromino
	 * 
	 * @throws 	IllegalArgumentException when width is not large enough to contain the 
	 * 			Tetromino
	 */
	public TetrominoGenerator(int width) throws IllegalArgumentException {
		if (width < 4) {
			throw new IllegalArgumentException();
		}
		this.width = width;
	}
	
	/**The generator takes a classic shape and builds the Tetromino based on the width 
	 * of the grid, the shape and the standard of the classic game.
	 * 
	 * @param 	shape the shape of the tetromino needed.
	 * 
	 * @throws 	IllegalStateException when no shape is passed and it tries to 
	 * 			build an unshaped Tetromino  
	 * 
	 */
	public Tetromino newTetromino(Shape shape) throws IllegalStateException {
		switch (shape) {
		case I:
			return new TetrominoImpl.Builder()
						.addPosition(new Point2D(width / 2 - 2, -1))
						.addShape(shape)
						.addColor(SquareColor.AZURE)
						.addPoint(new Point2D(0,1))
						.addPoint(new Point2D(1,1))
						.addPoint(new Point2D(2,1))
						.addPoint(new Point2D(3,1))
						.build();
		case L:
			return new TetrominoImpl.Builder()
						.addPosition(new Point2D(width / 2 - 1, 0))
						.addShape(shape)
						.addColor(SquareColor.ORANGE)
						.addPoint(new Point2D(1,0))
						.addPoint(new Point2D(1,1))
						.addPoint(new Point2D(1,2))
						.addPoint(new Point2D(2,2))
						.build();
		case J:
			return new TetrominoImpl.Builder()
						.addPosition(new Point2D(width / 2 - 1, 0))
						.addShape(shape)
						.addColor(SquareColor.BLUE)
						.addPoint(new Point2D(1,0))
						.addPoint(new Point2D(1,1))
						.addPoint(new Point2D(1,2))
						.addPoint(new Point2D(0,2))
						.build();
		case O:
			return new TetrominoImpl.Builder()
						.addPosition(new Point2D(width / 2 - 1, 0))
						.addShape(shape)
						.addColor(SquareColor.YELLOW)
						.addPoint(new Point2D(0,0))
						.addPoint(new Point2D(1,1))
						.addPoint(new Point2D(1,0))
						.addPoint(new Point2D(0,1))
						.build();
		case S:
			return new  TetrominoImpl.Builder()
						.addPosition(new Point2D(width / 2 - 1, 0))
						.addShape(shape)
						.addColor(SquareColor.GREEN)
						.addPoint(new Point2D(0,1))
						.addPoint(new Point2D(1,1))
						.addPoint(new Point2D(1,0))
						.addPoint(new Point2D(2,0))
						.build();
		case Z:
			return new TetrominoImpl.Builder()
						.addPosition(new Point2D(width / 2 - 1, 0))
						.addShape(shape)
						.addColor(SquareColor.RED)
						.addPoint(new Point2D(0,0))
						.addPoint(new Point2D(1,1))
						.addPoint(new Point2D(1,0))
						.addPoint(new Point2D(2,1))
						.build();
		case T:
			return new TetrominoImpl.Builder()
						.addPosition(new Point2D(width / 2 - 1, 0))
						.addShape(shape)
						.addColor(SquareColor.VIOLET)
						.addPoint(new Point2D(0,1))
						.addPoint(new Point2D(1,1))
						.addPoint(new Point2D(1,0))
						.addPoint(new Point2D(2,1))
						.build();
		default:
				return new TetrominoImpl.Builder().build();
		}
	}
}



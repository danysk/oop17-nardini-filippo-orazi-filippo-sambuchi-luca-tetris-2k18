package main.java.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TetrominoImpl implements Tetromino, Cloneable {
	private Point2D position;
	private List<Point2D> tetrom;
	private final SquareColor color;
	private int grid = 0;
	private Shape shape;
	 
	/**Private Class constructor it follows the builder pattern .
	 *
	 * @param pieces List of four point representing the single squares of a tetromino
	 * in a grid.
	 * @param position	point representing the position of the point (0,0) of the 
	 * pieces grid in the game matrix.
	 * @param color	a constant of the tetromino that represent a suggest color for it 
	 * but as no actual graphic meaning.
	 * @param shape one of the shape of the classic game.
	 */
	private TetrominoImpl(List<Point2D> pieces, Point2D position, SquareColor color, Shape shape) {
		tetrom = new ArrayList<Point2D>(pieces);
		this.position = new Point2D(position.getX(),position.getY());
		this.color = color;
		this.shape = shape;
		tetrom.forEach(p -> this.grid = Math.max(p.getX(),Math.max(p.getY(),grid)));
		this.grid++;
	}
	
	public Shape getShape() {
		return this.shape;
	}
	
	@Override
	public List<Square> getAllSquares() {
		List<Square> l = new LinkedList<>();
		tetrom.forEach(s -> l.add(
			new Square(s.getX() + position.getX(),s.getY() + position.getY(),this.color)));
		return l;
	}	
	
	@Override
	public SquareColor getColor() {
		return this.color;
	}
	
	@Override
	public void move(Direction dir) {
		switch (dir) {  
		case DOWN:
			position = new Point2D(position.getX(), position.getY() + 1);
			break;
		case LEFT:
			position = new Point2D(position.getX() - 1, position.getY());
			break;
		case RIGHT:
			position = new Point2D(position.getX() + 1, position.getY());
			break;
		case UP:
			position = new Point2D(position.getX(), position.getY() + 1);
			break;
		default:
		}
	}
	
	@Override
	public void rotate(RotationSense rotSense) {
		switch (rotSense) {
		case CLOCKWISE:
			List<Point2D> temp = new LinkedList<>();
			tetrom.forEach(p -> {
				temp.add(new Point2D(1 - (p.getY() - (grid - 2)), p.getX()));
			});
			tetrom = temp;
			break;
		case COUNTERCLOCKWISE:	
			for (int i = 0; i < 3; i++) {
				this.rotate(RotationSense.CLOCKWISE);
			}
			break;
		default:
		}
	}
	
	@Override
	public Tetromino clone() {
		return new TetrominoImpl(tetrom, position, color, shape);
	}
	
	 /**
	  * builder of the class Tetromino.
	  */
	public static class Builder {
		private Point2D position = new Point2D(0,0); 
		private List<Point2D> tetrom = new ArrayList<>();
		private SquareColor color = null;
		private Shape shape = null; 
		
		 /**
		  * class constructor.
		  */
		public Builder() {}
		 
		/** sets the position.
		  * 
		  * @param position	point representing the position of the point (0,0) of the 
		  * pieces grid in the game matrix.
		  * @return itself.
		  */
		public Builder addPosition(Point2D position) {
			this.position = position;
			return this;
		}
		
		 /**add a point in the inner grid.
		  * 
		  * @param piece the point to be added.
		  * @return itself.
		  * @throws IllegalArgumentException when 4 pieces have already been added.
		  */
		public Builder addPoint(Point2D piece) throws IllegalArgumentException {
			if (tetrom.size() == 4) {
				throw new IllegalArgumentException();
			}
			tetrom.forEach(p -> {
				if (p.getX() == piece.getX() && p.getY() == piece.getY()) {
					throw new IllegalArgumentException();
				}
			});			
			this.tetrom.add(piece);
			return this;
		}
		
		/**set the color.
		 * 
		 * @param color
		 * @return itself
		 */
		public Builder addColor(SquareColor color) {
			this.color = color;
			return this;
		}
		
		 /**set the shape.
		  * 
		  * @param shape
		  * @return itself
		  */
		public Builder addShape(Shape shape) {
			this.shape = shape;
			return this;
		}
		
		 /**build and create a new tetromino.
		  * 
		  * @return a new tetromino
		  * @throws IllegalStateException when there are less the 4 pieces 
		  */
		public Tetromino build() throws IllegalStateException {
			if (tetrom.size() != 4) {
				throw new IllegalStateException();
			}
			return new TetrominoImpl(tetrom, position, color, shape);
		}
		
			
	}

}

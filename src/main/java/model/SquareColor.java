package main.java.model;

/**enum representing the color of the tetramino 
 * this allows to separate the View from the Model.
 * The enum as no actual painting proprieties, the names derive 
 * from the classic game where those were the colors
 */
public enum SquareColor {
	AZURE,
	BLUE,
	RED,
	GREEN,
	YELLOW,
	VIOLET,
	ORANGE;
}

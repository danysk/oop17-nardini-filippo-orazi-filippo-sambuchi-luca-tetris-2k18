package main.java.model.queue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * It represents an always-full queue. Every time it is asked for the next 
 * object, it automatically fills the empty spot. It uses the strategy pattern
 * to fill the queue with a new value.
 */
public class AutofillableQueue<T> {
	
	private Queue<T> queue;
	private Supplier<T> supplier;

	/**
	 * Constructor for the queue.
	 * @param supplier supplier for the generation of the new values
	 * @param size size of the queue
	 */
	public AutofillableQueue(final Supplier<T> supplier, final int size) {
		this.supplier = supplier;	
		this.queue = new LinkedList<>(Stream.of(this.supplier.get())
			.limit(size)
			.collect(Collectors.toList()));
	}
	
	/**
	 * Getter for the queue.
	 */
	public List<T> getQueue() {
		return new ArrayList<>(queue);
	}
	
	/**
	 * Pulls the next element out of the queue and fills the empty spot with a
	 * new object obtained by the supplier.
	 */
	public T getNext() {
		this.queue.add(this.supplier.get());
		return this.queue.poll();
	}
}

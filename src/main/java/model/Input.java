package main.java.model;

/**
 * Represents the input that can be passed to the model.
 */
public enum Input {
	ROTATE_CW,
	ROTATE_CCW,
	MOVE_LEFT,
	MOVE_RIGHT,
	SOFT_DROP_ON,
	SOFT_DROP_OFF,
	HOLD;
}

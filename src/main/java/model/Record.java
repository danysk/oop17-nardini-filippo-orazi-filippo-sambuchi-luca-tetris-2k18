package main.java.model;

/**simple class used to store the information of the Players, doesn't 
 * allow a edit of the name or the score.
 * 
 */
public class Record {
	private final int score;
	private final String name;
	
	/**class Constructor.
	 * 
	 */
	public Record(String name, int score) {
		this.score = score;
		this.name = name;
	}
	
	/**getter for the score.
	 * 
	 * @return score value
	 */
	public int getScore() {
		return score;
	}
	
	/**getter for the name.
	 * 
	 * @return name string 
 	 */
	public String getName() {
		return name;
	}
	
}

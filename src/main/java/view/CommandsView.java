package main.java.view;


import static main.java.view.Dimension.HEIGHT;
import static main.java.view.Dimension.WIDTH;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;

/**
 * class that creates the option windows.
 */
public class CommandsView implements Displayable {

	private final BorderPane layout;
	private final Scene scene;
	private final Stage stage;

	/**
	 * Class constructor.
	 * Builds the view.
	 * @param stage
	 */
	public CommandsView(final Stage stage) throws Exception {
		layout = new BorderPane();
		layout.setTop(titleZone());;
		layout.setCenter(commandZone());
		layout.setBottom(bottomZone());
		scene = new Scene(layout,WIDTH.getValue(),HEIGHT.getValue());
		scene.getStylesheets().add("style.css");
		this.stage = stage;
	}
	
	/**
	 * builds the title layout.
	 * 
	 * @return the title layout
	 */
	private Node titleZone() {
        FlowPane tPane = new FlowPane(Orientation.HORIZONTAL,WIDTH.getValue() / 40,HEIGHT.getValue());
        tPane.setAlignment(Pos.TOP_CENTER);
		Label title = new Label("Commands");
		title.getStyleClass().addAll("outline","title");	
		tPane.getChildren().add(title);
		return tPane;
	}
	
	/**
	 * builds the commands zone layout.
	 * 
	 * @return the commands zone layout
	 */
	private Node commandZone() {
		FlowPane rPane = new FlowPane(Orientation.HORIZONTAL);
		rPane.setAlignment(Pos.CENTER);
		final ImageView imv = new ImageView();
        final Image image2 = new Image(getClass().getResourceAsStream("/comandi.png"));
        imv.setImage(image2);
        imv.setPreserveRatio(true);
        imv.setFitHeight(HEIGHT.getValue() / 1.80);	        
        rPane.getChildren().add(imv);

        
        return rPane;
	}
	
	/**
	 *  builds the bottom layout.
	 *  
	 * @return the bottom layout
	 */
	private Node bottomZone() {
		TilePane cPane = new TilePane();
		cPane.setPadding(new Insets(WIDTH.getValue() / 40,0,WIDTH.getValue() / 40,0));
		cPane.setAlignment(Pos.TOP_CENTER);
		cPane.setOrientation(Orientation.HORIZONTAL);
		Button credits = new Button("Credits");
		Button back = new Button("Back");
		credits.setOnAction(e -> {
			Stage creditsStage = new Stage();
			creditsStage.setTitle("Score board");
			 CreditsView scoreB = new CreditsView(creditsStage);	
			 creditsStage.setScene(scoreB.getScene());
			 creditsStage.show();		
		});
		back.setOnAction(e -> {
			stage.setScene(new MainWindows(stage).getScene());
		});
		cPane.setHgap(WIDTH.getValue() / 5);
		cPane.getChildren().addAll(credits,back);
		return cPane;
	}	
	

	public Scene getScene() {
		return scene;

	}

}

package main.java.view;

import java.awt.Toolkit;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CreditsView implements Displayable {
	private static double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 4;
	private static double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 1.50;	

	private Scene scene;
	private VBox layout;
	
	/**
	 * Class constructor.
	 */
	CreditsView(Stage stage) {
		layout = new VBox();
		Label title = new Label("Credits");
		title.getStyleClass().addAll("sub-title-label");
		Label msg = new Label("This nice and working game\nis brought you by:"
				+ "\nOrazi Filippo"
				+ "\nSambuchi Luca"
				+ "\nNardini Filippo"
				+ "\n\nhere's a nice picture");
		layout.getChildren().addAll(title,msg);
		final ImageView imv = new ImageView();
		final Image image2 = new Image(getClass().getResourceAsStream("/eg.PNG"));
		final Image image1 = new Image(getClass().getResourceAsStream("/eE.png"));
	  imv.setImage(image2);
    imv.setPreserveRatio(true);
    imv.setFitHeight(height / 5);
    imv.setOnMouseEntered(e -> imv.setImage(image1));
    imv.setOnMouseExited(e -> imv.setImage(image2));
    layout.getChildren().add(imv);
		Button exit = new Button("Exit");
		exit.setOnAction(e -> {
			stage.close();
		});
		
		layout.getChildren().addAll(exit);
		layout.setSpacing(height / 20);
		layout.setAlignment(Pos.TOP_CENTER);
		layout.setPadding(new Insets(height / 40,0, height / 40,0));
		scene = new Scene(layout, width, height);
		scene.getStylesheets().add("style.css");
	}

	@Override
	public Scene getScene() {
		return this.scene;
		}

}

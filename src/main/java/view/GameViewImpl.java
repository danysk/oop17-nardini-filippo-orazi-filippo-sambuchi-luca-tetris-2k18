package main.java.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.java.controller.GameLoop;
import main.java.model.Input;
import main.java.model.Shape;
import main.java.model.Square;
import main.java.model.SquareColor;
import main.java.model.Tetromino;
import main.java.model.TetrominoGenerator;

/**
 * Implementation of the main Game view. It implements Displayable so it can 
 * be displayed by the Stage. It uses the template method to map the model
 * Color class to the JavaFX one.
 */
public class GameViewImpl implements GameView, Displayable {
	
	private final Stage stage;
	//private final BorderPane main;
	private final HBox main;
	private final VBox status;
	private final VBox statusLeft;
	private static final int STD_SPACING = (int) (Dimension.HEIGHT.getValue() / 20);
	
	private final Canvas game;
	private final GraphicsContext gameGc;
	private static final Color BG_COLOR = Color.BLACK;
	private static final int HEIGHT = 20;
	private static final int WIDTH = 10;
	private static final int TILE_LENGTH = STD_SPACING;

	private final Label score;
	private final Label lines;
	private final Label level;
	
	private final Canvas next;
	private final GraphicsContext nextGc;
	private final Canvas hold;
	private final GraphicsContext holdGc;
	private static final int SIDE_TILE_LENGTH = (int) (TILE_LENGTH * 0.75);
	private static final int SIDE_HEIGHT = 4;
	private static final int SIDE_WIDTH = 4;
	
	private final List<Input> inputs;
	private final ColorMapper colorMapper = new ColorMapper() {
		private Map<SquareColor, Color> colors = new ImmutableMap.Builder<SquareColor, Color>()
				.put(SquareColor.AZURE, Color.LIGHTBLUE)
				.put(SquareColor.BLUE, Color.BLUE)
				.put(SquareColor.GREEN, Color.GREEN)
				.put(SquareColor.ORANGE, Color.ORANGE)
				.put(SquareColor.RED, Color.RED)
				.put(SquareColor.VIOLET, Color.VIOLET)
				.put(SquareColor.YELLOW, Color.YELLOW)
				.build();

		@Override
		public Color map(SquareColor c) {
			return colors.get(c);
		}
	};

	private final GameLoop gameLoop;

	/**
	 * Constructor of the View.
	 * @param gameLoop Reference to an existing GameLoop
	 * @param stage  Reference to the main stage
	 */
	public GameViewImpl(GameLoop gameLoop, Stage stage) {
		this.stage = stage;
		this.gameLoop = gameLoop;
		this.main = new HBox(STD_SPACING);
	
		Insets topPadding = new Insets(STD_SPACING, 0, 0, 0);
		this.status = new VBox(STD_SPACING / 2);
		this.status.getStyleClass().add("game-status");
		this.status.setPadding(topPadding);
		this.statusLeft = new VBox(STD_SPACING);
		this.statusLeft.getStyleClass().add("game-status");
		this.statusLeft.setPadding(topPadding);
		
		this.game = new Canvas(WIDTH * TILE_LENGTH, HEIGHT * TILE_LENGTH);
		this.gameGc = game.getGraphicsContext2D();
		this.gameGc.setFill(BG_COLOR);
		this.gameGc.fillRect(0, 0, game.getWidth(), game.getHeight());
		
		Insets labelPadding = new Insets(STD_SPACING * 0.50, STD_SPACING * 2,
		    STD_SPACING, STD_SPACING * 2);
		this.score = new Label();
		this.score.getStyleClass().add("game-label");
		this.score.setPadding(labelPadding);
		this.lines = new Label();
		this.lines.getStyleClass().add("game-label");
		this.lines.setPadding(labelPadding);
		this.level = new Label();
		this.level.getStyleClass().add("game-label");
		this.level.setPadding(labelPadding);
		
		this.next = new Canvas(SIDE_WIDTH * SIDE_TILE_LENGTH,
		    SIDE_HEIGHT * SIDE_TILE_LENGTH);
		this.nextGc = next.getGraphicsContext2D();
		this.nextGc.setFill(BG_COLOR);
		this.nextGc.fillRect(0, 0, this.next.getWidth(), this.next.getHeight());
		this.hold = new Canvas(SIDE_WIDTH * SIDE_TILE_LENGTH,
		    SIDE_HEIGHT * SIDE_TILE_LENGTH);
		this.holdGc = hold.getGraphicsContext2D();
		this.holdGc.setFill(BG_COLOR);
		this.holdGc.fillRect(0, 0, this.hold.getWidth(), this.hold.getHeight());
		
		this.status.getChildren().addAll(new Label("Next"), next, new Label("Score"),
		    score, new Label("Lines"), lines, new Label("Level"), level);
		this.statusLeft.getChildren().addAll(new Label("Hold"), hold);
		
		this.main.getChildren().addAll(statusLeft, game, status);
		this.main.setAlignment(Pos.CENTER);
		
		this.inputs = new ArrayList<>();
	}

  /**
   * Updates the current score.
   */
	@Override
	public void updateScore(int score) {
		this.score.setText(new Integer(score).toString());
	}

	 /**
   * Updates the number of lines cleared.
   */
	@Override
	public void updateLines(int lines) {
		this.lines.setText(new Integer(lines).toString());		
	}

	 /**
   * Updates the current level.
   */
	@Override
	public void updateLevel(int level) {
		this.level.setText(new Integer(level).toString());		
	}
	
  /**
   * Draws the game board.
   */
  @Override
  public void updateBoard(List<Square> board) {
    this.gameGc.setFill(BG_COLOR);
    this.gameGc.fillRect(0, 0, this.game.getWidth(), this.game.getHeight());
    board.forEach(this::draw);
  }
  
  /**
   * Updates the next tetromino in the queue.
   */
  @Override
  public void updateNext(Shape shape) {
    this.nextGc.setFill(BG_COLOR);
    this.nextGc.fillRect(0, 0, this.next.getWidth(), this.next.getHeight());
    Tetromino t = new TetrominoGenerator(4).newTetromino(shape);
    t.getAllSquares().forEach(s -> {
      this.nextGc.setFill(colorMapper.map(s.getColor()));
      this.nextGc.fillRoundRect(s.getCoords().getX() * SIDE_TILE_LENGTH,
            s.getCoords().getY() * SIDE_TILE_LENGTH, SIDE_TILE_LENGTH, SIDE_TILE_LENGTH, 10, 10);
    });
  }
  
  /**
   * Updates the tetromino used by the hold power-up.
   */
  @Override
  public void updateHold(Shape shape) {
    this.holdGc.setFill(BG_COLOR);
    this.holdGc.fillRect(0, 0, this.next.getWidth(), this.next.getHeight());
    Tetromino t = new TetrominoGenerator(4).newTetromino(shape);
    t.getAllSquares().forEach(s -> {
      this.holdGc.setFill(colorMapper.map(s.getColor()));
      this.holdGc.fillRoundRect(s.getCoords().getX() * SIDE_TILE_LENGTH,
            s.getCoords().getY() * SIDE_TILE_LENGTH, SIDE_TILE_LENGTH, SIDE_TILE_LENGTH, 10, 10);
    });
  }

  /**
   * Returns a list of the inputs that the view component catches.
   */
	@Override
	public List<Input> getInputs() {
		List<Input> t = new ArrayList<>(inputs);
		inputs.clear();
		return t;
	}

  /**
   * Returns the scene to display.
   */
	@Override
	public Scene getScene() {
		Scene s = new Scene(main, Dimension.WIDTH.getValue(), Dimension.HEIGHT.getValue());
		s.getStylesheets().add("style.css");
		s.setOnKeyPressed(this::resolveKeyPressed);
		s.setOnKeyReleased(this::resolveKeyReleased);
        return s;
	}
	
	/**
	 * Draws the given square into the game canvas. 
	 */
	private void draw(Square square) {
		this.gameGc.setFill(colorMapper.map(square.getColor()));
        this.gameGc.fillRoundRect(square.getCoords().getX() * TILE_LENGTH,
        		square.getCoords().getY() * TILE_LENGTH, TILE_LENGTH, TILE_LENGTH, 10, 10);
    }
	
	/**
	 * Resolves a KeyPressed event.
	 */
	private void resolveKeyPressed(KeyEvent e) {
		switch (e.getCode()) {
		case ESCAPE:
			this.gameLoop.togglePause();
			this.onPause();
			break;
		case LEFT:
			inputs.add(Input.MOVE_LEFT);
			break;
		case RIGHT:
			inputs.add(Input.MOVE_RIGHT);
			break;
		case DOWN:
			inputs.add(Input.SOFT_DROP_ON);
			break;
		case D:
			inputs.add(Input.ROTATE_CCW);
			break;
		case F:
			inputs.add(Input.ROTATE_CW);
			break;
		case SPACE:
			inputs.add(Input.HOLD);
			break;
		default:
		}
	}
	
	 /**
   * Resolves a KeyReleased event.
   */
	private void resolveKeyReleased(KeyEvent e) {
		switch (e.getCode()) {
		case DOWN:
			inputs.add(Input.SOFT_DROP_OFF);
			break;
		default:
		}
	}
	
	/**
	 * Triggered by the ESC key, it sets the pause view.
	 */
	private void onPause() {
		this.stage.setScene(new PauseView(stage,gameLoop).getScene());
	}

}

package main.java.view;

import java.awt.Toolkit;

public enum Dimension {

  WIDTH(Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 1.5),
  HEIGHT(Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 1.25);

  private double value;

  private Dimension(double value) {
    this.value = value;
  }

  public double getValue() {
    return this.value;
  }

}

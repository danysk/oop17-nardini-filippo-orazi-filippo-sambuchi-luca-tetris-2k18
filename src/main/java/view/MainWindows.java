package main.java.view;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import main.java.controller.GameLoopImpl;
import main.java.model.Tetris;
import main.java.model.TetrisImpl;

import static main.java.view.Dimension.HEIGHT;
import static main.java.view.Dimension.WIDTH;

import javafx.scene.Node;

public class MainWindows implements Displayable {

	private final BorderPane layout;
	private final Stage stage;
	private final Scene scene;

	/**
	 * Costruttor.
	 * 
	 */
	public MainWindows(final Stage stage) {
		this.stage = stage;
		layout = new BorderPane();
		layout.setTop(titleZone());
		layout.setLeft(null);

		layout.setCenter(centerZone());
		layout.setBottom(null);
		scene = new Scene(layout, WIDTH.getValue(), HEIGHT.getValue());
		scene.getStylesheets().add("style.css");
	}

	/**
	 * 
	 * Returns a Node that contains the CenterZone.
	 */
	public Node centerZone() {

		final TilePane cPane = new TilePane();
		cPane.setOrientation(Orientation.VERTICAL);
		cPane.setAlignment(Pos.CENTER);
		cPane.setVgap(25);

		final Button singlePlayer = new Button("SINGLE PLAYER");
		final Button highscore = new Button("HIGHSCORE");
		final Button options = new Button("COMMANDS");
		final Button exit = new Button("EXIT");

		singlePlayer.setOnAction(e -> {
			final Tetris t = new TetrisImpl(10, 20);
			final GameLoopImpl gL = new GameLoopImpl(t, stage);
			stage.setScene(gL.getView().getScene());
			gL.start();
		});

		highscore.setOnAction(e -> {
			final Stage scoreBoard = new Stage();
			scoreBoard.setTitle("Score board");
			ScoreboardWindow scoreB = new ScoreboardWindow(scoreBoard);
			scoreBoard.setScene(scoreB.getScene());
			scoreBoard.show();
		});

		options.setOnAction(e -> {
			try {
				stage.setScene(new CommandsView(stage).getScene());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});

		exit.setOnAction(e -> {
			System.exit(1);
		});

		singlePlayer.getStyleClass().add("buttonsMenu");
		highscore.getStyleClass().add("buttonsMenu");
		options.getStyleClass().add("buttonsMenu");
		exit.getStyleClass().add("buttonsMenu");

		cPane.getChildren().addAll(singlePlayer, highscore, options, exit);

		return cPane;
	}

	/**
	 * 
	 * Returns a Node that contains the TitleZone.
	 */
	public Node titleZone() {
		final FlowPane tPane = new FlowPane(
		                        Orientation.HORIZONTAL, WIDTH.getValue() / 40,
		                        HEIGHT.getValue() / 30);
		tPane.setAlignment(Pos.TOP_CENTER);
		final Label title = new Label("TETRIS");
		title.getStyleClass().add("mainTitle");
		tPane.getChildren().add(title);
		return tPane;
	}

	@Override
	public Scene getScene() {
		return scene;
	}

}
